import React, { useEffect, useState } from 'react'
import firebase from './firebase'

export default function ListingExpansionPanels() {
  const dbRef = firebase.database().ref('2020').orderByChild('trueDate')

  const [data, setData] = useState([])

  useEffect(() => {
    dbRef.on('value', (snapshot) => {
      let dataCopy = []

      snapshot.forEach(function (item) {
        dataCopy.push({
          key: item.key,
          electricite: item.val().electricite,
          gaz: item.val().gaz,
          formatedDate: item.val().formatedDate,
        })
      })

      setData(dataCopy)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      {data.map((item, key) => {
        return <b key={key}>{JSON.stringify(item)}</b>
      })}
    </>
  )
}
