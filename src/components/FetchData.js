import { useState, useEffect } from 'react'
import firebase from 'firebase'

export default function FetchData() {
  // FETCH DATA
  const dbRef = firebase.database().ref('2020').orderByChild('trueDate')

  const [listingData, setListingData] = useState([])

  useEffect(() => {
    dbRef.on('value', (snapshot) => {
      let dataCopy = []

      snapshot.forEach(function (item) {
        dataCopy.push({
          key: item.key,
          electricite: item.val().electricite,
          gaz: item.val().gaz,
          formatedDate: item.val().formatedDate,
        })
      })

      setListingData(dataCopy)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return listingData
}
