import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import moment from 'moment'
import 'moment/locale/fr'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import firebase from '../firebase'

export default function AddForm() {
  // MONTHS
  moment.locale('fr')
  const [selectedDate, handleDateChange] = useState(new Date())
  const trueDate = moment(selectedDate).format('YYYY-MM-DD')
  const formatedDate = moment(selectedDate).format('MMMM YYYY')

  // ELECTRICITÉ
  const [kw, setKw] = useState('')
  const handleUpdateKw = function (event) {
    setKw(event.target.value)
  }

  // GAZ
  const [m3, setm3] = useState('')
  const handleUpdateGaz = function (event) {
    setm3(event.target.value)
  }

  // OPEN CLOSE MODAL
  const [open, setOpen] = useState(false)
  const handleClickOpenAddDialog = () => {
    setOpen(true)
  }
  const handleCloseAddDialog = () => {
    setOpen(false)
  }

  // SUBMIT
  const dbRef = firebase.database().ref('2020')
  const handleSubmit = () => {
    const releve = {
      trueDate: trueDate,
      formatedDate: formatedDate,
      gaz: Number(m3),
      electricite: Number(kw),
    }
    dbRef.push(releve)
    setm3('')
    setKw('')
    setOpen(false)
  }

  return (
    <>
      <div>
        <Fab
          className='fab-btn-add'
          color='primary'
          aria-label='add'
          onClick={handleClickOpenAddDialog}
        >
          <AddIcon />
        </Fab>
        <Dialog
          open={open}
          onClose={handleCloseAddDialog}
          aria-labelledby='alert-dialog-title'
          aria-describedby='alert-dialog-description'
        >
          <DialogTitle id='alert-dialog-title'>Entrer les valeurs</DialogTitle>
          <DialogContent>
            <form className='addform' noValidate autoComplete='off'>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <DatePicker
                  inputVariant='outlined'
                  value={selectedDate}
                  onChange={handleDateChange}
                  label='Mois'
                  format='MMMM Y'
                  views={['month']}
                  minDate={new Date('2020-01-01')}
                  maxDate={new Date('2020-12-31')}
                />
              </MuiPickersUtilsProvider>
              <TextField
                label='Electricité'
                variant='outlined'
                type='number'
                InputProps={{
                  endAdornment: (
                    <InputAdornment position='end'>kW</InputAdornment>
                  ),
                }}
                value={kw}
                onChange={handleUpdateKw}
              />
              <TextField
                label='Gaz'
                variant='outlined'
                type='number'
                InputProps={{
                  endAdornment: (
                    <InputAdornment position='end'>m3</InputAdornment>
                  ),
                }}
                value={m3}
                onChange={handleUpdateGaz}
              />
              <Button
                className='btn-submit'
                size='large'
                variant='contained'
                color='primary'
                startIcon={<CloudUploadIcon />}
                onClick={handleSubmit}
              >
                Confirmer
              </Button>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseAddDialog} color='primary' autoFocus>
              Retour
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  )
}
