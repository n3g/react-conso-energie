import React from 'react'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Chip from '@material-ui/core/Chip'

const Month2020 = ({
  listingData,
  i,
  elecTotalPrice,
  diffElecPercent,
  diffGazPercent,
  gazTotalPrice,
  handleDelete,
}) => {
  return (
    <div>
      <Typography className='expansion-detail' component='span'>
        <Grid container>
          <Grid item xs={12}>
            <b
              style={{
                color: '#cacaca',
              }}
            >
              2020
            </b>
          </Grid>
          <Grid item xs>
            <Icon>power</Icon>
            <span>
              {listingData[i].electricite} <small>Kwh</small>
            </span>
            <div>
              <Chip className='' size='small' label={elecTotalPrice + ' €'} />
              {i === 0
                ? []
                : [
                    <Chip
                      key={listingData[i].key}
                      className={diffElecPercent > 0 ? 'plus' : 'moins'}
                      size='small'
                      label={diffElecPercent + ' %'}
                    />,
                  ]}
            </div>
          </Grid>
          <Grid item xs>
            <Icon>whatshot</Icon>
            <span>
              {listingData[i].gaz}{' '}
              <small>
                m<sup>3</sup>
              </small>
            </span>
            <div>
              <Chip className='' size='small' label={gazTotalPrice + ' €'} />
              {i === 0
                ? []
                : [
                    <Chip
                      key={listingData[i].key}
                      className={diffGazPercent > 0 ? 'plus' : 'moins'}
                      size='small'
                      label={diffGazPercent + ' %'}
                    />,
                  ]}
            </div>
          </Grid>
          <Grid item xs={12}>
            <Button
              className='btnRemove'
              style={{
                marginTop: 15,
              }}
              size='small'
              color='secondary'
              onClick={() => handleDelete(listingData[i].key)}
            >
              Supprimer
            </Button>
          </Grid>
        </Grid>
      </Typography>
    </div>
  )
}

export default Month2020
