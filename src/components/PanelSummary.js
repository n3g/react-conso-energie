import React from 'react'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'

const PanelSummary = ({ listingData, i, total }) => {
  return (
    <ExpansionPanelSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls={listingData[i].key}
      id={listingData[i].key}
    >
      <Typography>
        <span>
          <Icon
            style={{
              marginRight: 15,
            }}
          >
            calendar_today
          </Icon>
          <span className='date'>{listingData[i].formatedDate}</span>
          <span className='total'>{total}</span>
        </span>
      </Typography>
    </ExpansionPanelSummary>
  )
}

export default PanelSummary
