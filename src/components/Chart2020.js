import React from 'react'
import Chart from 'react-apexcharts'

export default function Chart2020(listingData) {
  console.log(listingData)

  var months = [1, 2, 3]
  var gazValues = [4, 5, 6]

  const chartOptions = {
    chart: {
      id: 'basic-bar',
    },
    xaxis: {
      categories: months,
    },
    theme: {
      mode: 'dark',
      palette: 'palette7',
    },
  }

  const chartSeries = [
    {
      name: 'conso',
      data: gazValues,
    },
  ]

  return (
    <Chart
      options={chartOptions}
      series={chartSeries}
      type='line'
      width='100%'
    />
  )
}
