import React from 'react'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import firebase from './firebase'
import PanelSummary from './components/PanelSummary'
import SwipeableViews from 'react-swipeable-views'
import Month2020 from './components/Month2020'

export default function ListingExpansionPanels({ listingData }) {
  // DELETE
  const handleDelete = (key) => {
    firebase.database().ref(`2020/${key}`).remove()
  }

  // FIX VALUES
  const coefCity = 11.34,
    subElecYear = 117.56,
    subGazYear = 215.73,
    kwhPrice = 0.15356,
    m3Price = 0.0681,
    lastYearGazIndex = 731.89,
    lastYearElecIndex = 4695

  // DISPLAY DATA
  const dataItem = []

  var elecTotalPrice = '',
    gazTotalPrice = '',
    total = '',
    prevElecTotalPrice = '',
    prevGazTotalPrice = '',
    diffElecPercent = '',
    diffGazPercent = ''

  const getElecTotalPrice = (month, lastRef) => {
    return Number(
      (
        (listingData[month].electricite - lastRef) * kwhPrice +
        subElecYear / 12
      ).toFixed(2)
    )
  }

  const getGazTotalPrice = (month, lastRef) => {
    return Number(
      (
        (listingData[month].gaz - lastRef) * coefCity * m3Price +
        subGazYear / 12
      ).toFixed(2)
    )
  }

  const diffAmountToPercent = (month, prevMonth) => {
    return Number(((month * 100) / prevMonth - 100).toFixed(2))
  }

  for (let i = 0; i < listingData.length; i++) {
    // If january, different behaviour
    if (i === 0) {
      elecTotalPrice = getElecTotalPrice(i, lastYearElecIndex)
      gazTotalPrice = getGazTotalPrice(i, lastYearGazIndex)
    } else {
      elecTotalPrice = getElecTotalPrice(i, listingData[i - 1].electricite)
      gazTotalPrice = getGazTotalPrice(i, listingData[i - 1].gaz)

      // Get previous total
      if (i === 1) {
        prevElecTotalPrice = getElecTotalPrice(i - 1, lastYearElecIndex)
        prevGazTotalPrice = getGazTotalPrice(i - 1, lastYearGazIndex)
      } else {
        prevElecTotalPrice = getElecTotalPrice(
          i - 1,
          listingData[i - 2].electricite
        )
        prevGazTotalPrice = getGazTotalPrice(i - 1, listingData[i - 2].gaz)
      }
    }

    total = (elecTotalPrice + gazTotalPrice).toFixed(2) + '€'
    diffElecPercent = diffAmountToPercent(elecTotalPrice, prevElecTotalPrice)
    diffGazPercent = diffAmountToPercent(gazTotalPrice, prevGazTotalPrice)

    dataItem.push(
      <ExpansionPanel key={listingData[i].key} className='relevePanel'>
        <PanelSummary listingData={listingData} i={i} total={total} />
        <ExpansionPanelDetails>
          <SwipeableViews>
            <Month2020
              listingData={listingData}
              i={i}
              elecTotalPrice={elecTotalPrice}
              diffElecPercent={diffElecPercent}
              diffGazPercent={diffGazPercent}
              gazTotalPrice={gazTotalPrice}
              handleDelete={handleDelete}
            />
          </SwipeableViews>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }

  return <>{dataItem}</>
}
