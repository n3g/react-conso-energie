import React from 'react'
import AddForm from './components/AddForm'
import ListingExpansionPanels from './ListingExpansionPanels'
import { CssBaseline } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import FetchData from './components/FetchData'

import './styles.css'

export default function App() {
  return (
    <>
      <CssBaseline />
      <div className='App'>
        <Grid container justify='center' spacing={2}>
          <Grid item xs={12}>
            <h1>Conso Energie</h1>
            <AddForm />
          </Grid>
          <Grid item xs={12}>
            <ListingExpansionPanels listingData={FetchData()} />
          </Grid>
        </Grid>
      </div>
    </>
  )
}
