import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyDOs9CtEG4_cVAF9JezhLZhTwb6L8jchDA',
  authDomain: 'codesandboxexample.firebaseapp.com',
  databaseURL: 'https://codesandboxexample.firebaseio.com',
  projectId: 'codesandboxexample',
  storageBucket: 'codesandboxexample.appspot.com',
  messagingSenderId: '1097020554558',
  appId: '1:1097020554558:web:295a93db10ac68616c406f',
}

firebase.initializeApp(config)

export default firebase
