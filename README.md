# TodoList
* [ ] Feedback SnackBar pour suppression des données
* [ ] Swiper sur les relevés pour afficher les graphs
* [ ] Ajouter swipe dans un relevé pour switcher d'année
* [ ] Construire les données 2019
* [ ] Revoir les calculs, ne plus stocker le total € en base (le calculer plutôt)
* [ ] Revoir la façon d'appeler et de boucler dans les valeurs
 
# Done
* [x] Gitlab CI / Gitlab Pages
* [x] Connexion avec Firebase
* [x] Ajout des données
* [x] Suppression des données
* [x] Material UI
* [x] Feedback SnackBar pour ajout des données
* [x] Afficher les détails des relevés au click